class Question {
  String category = '';
  String type = '';
  String difficulty = '';
  String question = '';
  String correctAnswer = '';
  List<String> incorrectAnswers = [];

  Question(this.category, this.type, this.difficulty, this.question,
      this.correctAnswer, this.incorrectAnswers);

  Question.fromJson(Map<String, dynamic> questionMap) {
    category = questionMap['category'] ?? '';
    type = questionMap['type'] ?? '';
    difficulty = questionMap['difficulty'] ?? '';
    question = questionMap['question'] ?? '';
    correctAnswer = questionMap['correct_answer'] ?? '';
    incorrectAnswers = List<String>.from(questionMap['incorrect_answers'] as List);
  }
}
