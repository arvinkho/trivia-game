import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:trivia_game/data/question.dart';

class HttpHelper {
  // https://opentdb.com/api_category.php
  final String authority = 'opentdb.com';
  final String categoryPath = 'api_category.php';
  final String apiPath = 'api.php';

  Future<Map<String, dynamic>> getCategories() async {
    Uri uri = Uri.https(authority, categoryPath);
    http.Response result = await http.get(uri);
    Map<String, dynamic> data = json.decode(result.body);

    Map<String, dynamic> categories = Map();
    for (final element in data['trivia_categories']) {
      categories[element['name']] = element['id'];
    }

    return categories;
  }

//CHANGE DATA TYPE TO QUESTION
  Future<List<Question>> getQuestions(
      String numOfQs, String? difficulty, String? category) async {
    Map<String, dynamic> parameters = {
      'amount': numOfQs,
      'category': category,
      'difficulty': difficulty,
    };
    Uri uri = Uri.https(authority, apiPath, parameters);
    print(uri);
    http.Response result = await http.get(uri);
    Map<String, dynamic> data = json.decode(result.body);
    List<Question> questions = [];
    for (final element in data['results']) {
      questions.add(Question.fromJson(element));
    }
    // print(questions[0].question);
    // print(data);
    return questions;
  }
}
