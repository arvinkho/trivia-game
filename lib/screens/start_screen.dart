// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:trivia_game/data/http_helper.dart';
import 'package:trivia_game/data/question.dart';
import 'package:trivia_game/screens/question_screen.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  String? _name;
  String? _difficulty;
  String? _numOfQs;
  Map<String, dynamic>? _categories;
  String? _category;
  late List<Question> _questions;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
        decoration: InputDecoration(labelText: 'Name'),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Name is required';
          }
        },
        onSaved: (value) {
          _name = value ?? '';
        });
  }

  Widget _buildNumOfQs() {
    return DropdownButtonFormField(
      decoration: InputDecoration(labelText: 'Number of questions'),
      items: <String>[
        '5',
        '10',
        '15',
        '20',
      ]
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (String? value) {
        _numOfQs = value ?? 'Any';
      },
    );
  }

  Widget _buildDifficulty() {
    return DropdownButtonFormField(
      decoration: InputDecoration(labelText: 'Difficulty'),
      items: <String>[
        'Any',
        'Easy',
        'Medium',
        'Hard',
      ]
          .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  ))
          .toList(),
      onChanged: (String? value) {
        _difficulty = value ?? 'Any';
      },
    );
  }

  Widget _buildGenre() {
    if (_categories == null) {
      getData();
      return DropdownButtonFormField(
        decoration: InputDecoration(labelText: 'Question Category'),
        items: <String>[
          'Loading',
        ]
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (String? value) {
          _difficulty = value ?? 'Any';
        },
      );
    } else {
      return DropdownButtonFormField(
        decoration: InputDecoration(labelText: 'Question Category'),
        items: (<String>['Random'] + _categories!.keys.toList())
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (String? value) {
          _category = value ?? 'Any';
        },
      );
    }
  }

  final TextEditingController txtUsername = TextEditingController();
  String username = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Trivia Game'),
      ),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Flexible(child: _buildName()),
              Flexible(child: _buildDifficulty()),
              Flexible(child: _buildGenre()),
              Flexible(child: _buildNumOfQs()),
              SizedBox(
                height: 100,
              ),
              Flexible(
                child: ElevatedButton(
                  child: Text(
                    'Submit',
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 16,
                    ),
                  ),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }
              
                    _formKey.currentState!.save();
                    getQuestions();
              
                    // BUILD URI STRING
                    // SEND REQUEST
                    // PUSH TO NEXT PAGE 😁
                    print(_name);
                    print(_difficulty);
                    print(_category);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future getData() async {
    HttpHelper helper = HttpHelper();
    _categories = await helper.getCategories();
    setState(() {});
  }

  Future getQuestions() async {
    HttpHelper helper = HttpHelper();

    _questions = await helper.getQuestions(_numOfQs!, _difficulty == 'Any' ? '' : _difficulty,
        _category == 'Random' ? '' : _categories?[_category].toString());
    setState(() {});
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => QuestionScreen(_questions)));
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text('Trivia Game'),
  //     ),
  //     body: Container(
  //       decoration: BoxDecoration(
  //         image: DecorationImage(
  //           image: AssetImage('assets/trivia_night.jpg'),
  //           fit: BoxFit.cover,
  //         ),
  //       ),
  //       child: Padding(
  //         padding: const EdgeInsets.all(32),
  //         child: Center(
  //           child: Container(
  //             alignment: Alignment.bottomCenter,
  //             padding: EdgeInsets.only(bottom: 32),
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: [
  //                 Row(
  //                   children: [
  //                     Expanded(
  //                       child: TextField(
  //                         controller: txtUsername,
  //                         decoration: InputDecoration(
  //                           hintText: 'Please enter your user name',
  //                           hintStyle: TextStyle(
  //                             fontSize: 16,
  //                             color: Colors.black,
  //                           ),
  //                           labelText: 'User name',
  //                           labelStyle: TextStyle(
  //                             fontSize: 24,
  //                             color: Colors.black,
  //                           ),
  //                           border: OutlineInputBorder(
  //                               borderSide: BorderSide(width: 2),
  //                               borderRadius:
  //                                   BorderRadius.all(Radius.circular(30))),
  //                           fillColor: Colors.white70,
  //                           filled: true,
  //                         ),
  //                       ),
  //                     ),
  //                     ElevatedButton(
  //                       child: Text('Next'),
  //                       onPressed: doNothing,
  //                     ),
  //                   ],
  //                 )
  //               ],
  //             ),
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }
}
