import 'package:flutter/material.dart';
import 'package:trivia_game/screens/result_screen.dart';
import '../data/question.dart';

class QuestionScreen extends StatefulWidget {
  final List<Question> questions;
  const QuestionScreen(List<Question> this.questions, {Key? key})
      : super(key: key);

  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  Question? _currentQuestion;
  int _index = 0;
  int _numOfQuestions = 0;
  int _numOfCorrectAnswers = 0;
  List<String> _answers = [];
  bool _correctAnswer = false;

  @override
  Widget build(BuildContext context) {
    _numOfQuestions = widget.questions.length;

    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 110, left: 20, right: 20),
          child: renderQuestion(),
        ),
      ),
    );
  }

  Column renderQuestion() {
    String alt1, alt2, alt3, alt4 = '';
    bool multipleChoice = false;
    List<String> alternatives = [];

    if (_index < _numOfQuestions) {
      _currentQuestion = widget.questions[_index];
      multipleChoice = (_currentQuestion!.type == 'boolean' ? false : true);
      alternatives = [_currentQuestion!.correctAnswer] +
          _currentQuestion!.incorrectAnswers;
      alternatives.shuffle();
    } 

    if (multipleChoice) {
      return Column(
        children: [
          Text('Question ${_index + 1}: ${_currentQuestion!.question}'),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ElevatedButton(
                child: Text(alternatives[0]),
                onPressed: () => {
                  validateAnswer(alternatives[0]),
                },
              ),
              ElevatedButton(
                child: Text(alternatives[1]),
                onPressed: () => {
                  validateAnswer(alternatives[1]),
                },
              ),
              ElevatedButton(
                child: Text(alternatives[2]),
                onPressed: () => {
                  validateAnswer(alternatives[2]),
                },
              ),
              ElevatedButton(
                child: Text(alternatives[3]),
                onPressed: () => {
                  validateAnswer(alternatives[3]),
                },
              ),
            ],
          ),
        ],
      );
    } else {
      return Column(
        children: [
          Text('Question ${_index + 1}: ${_currentQuestion!.question}'),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ElevatedButton(
                child: Text(alternatives[0]),
                onPressed: () => {
                  validateAnswer(alternatives[0]),
                },
              ),
              ElevatedButton(
                child: Text(alternatives[1]),
                onPressed: () => {
                  validateAnswer(alternatives[1]),
                },
              ),
            ],
          ),
        ],
      );
    }
  }

  bool validateAnswer(String answer) {
    _correctAnswer = (answer == _currentQuestion!.correctAnswer);
    _answers.add(answer);
    if (_correctAnswer) {
      _numOfCorrectAnswers++;
    }
    print(_correctAnswer);
    notifyResult();
    return _correctAnswer;
  }

  void notifyResult() {
    String result = _correctAnswer == true ? 'Correct!' : 'Incorrect';
    _index++;
    if (_index < _numOfQuestions) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(result),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.pop(context, 'OK');
                    
                    setState(() {});
                  },
                  child: const Text('Next Question'),
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(result),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ResultScreen(
                            widget.questions, _answers, _numOfCorrectAnswers),
                      ),
                    );
                  },
                  child: const Text('View results'),
                ),
              ],
            );
          });
    }
  }
}

// return Scaffold(
//       appBar: AppBar(
//         title: Text('Question'),
//       ),
//       body: GridView.count(
//         crossAxisCount: 2,
//         children: [
//           Center(child: Text('Button 1')),
//           Center(child: Text('Button 2')),
//           Center(child: Text('Button 3')),
//           Center(child: Text('Button 4'))
//         ],
//       ),
//     );
//   }