import 'package:flutter/material.dart';
import 'package:trivia_game/data/question.dart';

class ResultScreen extends StatefulWidget {
  final List<Question> questions;
  final List<String> answers;
  final int numOfCorrectAnswers;

  const ResultScreen(this.questions, this.answers, this.numOfCorrectAnswers,
      {Key? key})
      : super(key: key);

  @override
  State<ResultScreen> createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  @override
  Widget build(BuildContext context) {
    List<Question> _questions = widget.questions;
    List<String> _answers = widget.answers;
    int _numOfCorrectAnswers = widget.numOfCorrectAnswers;
    List<Widget> resultCards = [];

    for (int i = 0; i < _questions.length; i++) {
      resultCards.add(Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(child: _buildResultCard(_questions[i], _answers[i])),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: GridView.count(
        childAspectRatio: (1 / .75),
        crossAxisCount: 1,
        children: resultCards,
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  Widget _buildResultCard(Question question, String answer) {
    bool correctAnswer = answer == question.correctAnswer ? true : false;
    Color color = answer == question.correctAnswer ? Colors.green : Colors.red;
    Color accentColor = answer == question.correctAnswer
        ? Colors.greenAccent
        : Colors.redAccent;

    List<Widget> answersWidgets = [
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Text(question.correctAnswer),
        ),
      )
    ];
    for (final element in question.incorrectAnswers) {
      answersWidgets.add(Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Text(element),
        ),
      ));
    }
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: color
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
              Expanded(
                child: Text(
                  question.question,
                  style: TextStyle(fontSize: 20, overflow: TextOverflow.visible),
                ),
              ),
              SizedBox(height: 10),
            ] +
            answersWidgets,
      ),
    );
    // return Card(
    //     child: GridTile(
    //   header: GridTileBar(
    //     backgroundColor: color,
    //     title: Text(question.question, overflow: TextOverflow.,),
    //   ),
    //   child: Column(
    //     children: answersWidgets,
    //   ),
    // ));
  }
}
