// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:trivia_game/data/http_helper.dart';
import 'package:trivia_game/screens/question_screen.dart';
import 'package:trivia_game/screens/start_screen.dart';

void main() {
  runApp(const TriviaApp());
}

class TriviaApp extends StatelessWidget {
  const TriviaApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      initialRoute: '/',
      routes: {
        '/': (context) => StartScreen(),
      },
    );
  }
}
